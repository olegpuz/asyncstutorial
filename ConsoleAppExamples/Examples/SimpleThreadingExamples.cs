﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ConsoleAppExamples.Examples
{
    public static class SimpleThreadingExamples
    {
        /// <summary>
        /// 2 threads async
        /// </summary>
        public static void Example1()
        {
            var p1 = new LetterPrinter('A');
            var p2 = new LetterPrinter('B');
            new Thread(p1.Print).Start();
            new Thread(p2.Print).Start();
        }

        /// <summary>
        /// 2 threads using same storage
        /// </summary>
        public static void Example2()
        {
            var p1 = new LetterPrinter('A');
            new Thread(p1.Print).Start();
            for (int i = 0; i < 30; i++)
            {
                p1.Letter++;
                Thread.Sleep(1);
            }
            p1.Print();
        }

        /// <summary>
        /// Synchronized running.
        /// </summary>
        public static void Example3()
        {
            var locker = new object();
            var p1 = new LetterPrinter('A');

            void LocalFuncPrint()
            {
                lock (locker)
                {
                    p1.Print();
                    p1.Letter++;
                }
            }

            new Thread(LocalFuncPrint).Start();
            LocalFuncPrint();
        }

        /// <summary>
        /// Exceptions catching
        /// </summary>
        public static void Example4()
        {
            new Thread(() => throw null).Start();
        }

        /// <summary>
        /// Join, Abort, Interrupt example
        /// </summary>
        public static void Example5()
        {
            //TODO
        }
    }
}