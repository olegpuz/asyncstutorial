﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ConsoleAppExamples.Examples
{
    public static class AsynchronousExamples
    {
        private static Random Rand = new Random();
        /// <summary>
        /// Doing CPU bound asynchronous work
        /// </summary>
        public static void Example1()
        {
            var bigArray = new char[1065041023];
            var valuesWithIndexRange = GenerateRangeForValues("123456".ToCharArray(), bigArray.Length);

            //var bigArray = new bool[2130082046];
            //var valuesWithIndexRange = GenerateRangeForValues(Enumerable.Repeat(true, 6).ToArray(), bigArray.Length);

            //var bigArray = new object[266260255];
            //var valuesWithIndexRange = GenerateRangeForValues(Enumerable.Repeat(new object(), 6).ToArray(), bigArray.Length);


            //assigning values at random position per piece
            valuesWithIndexRange.ForEach(t => bigArray[Rand.Next(t.Item2, t.Item3)] = t.Item1);
            //initializing threads
            var threads = valuesWithIndexRange.Select(t => new Thread(() => SearchValue(bigArray, t.Item1, t.Item2, t.Item3))).ToList();
            //starting threads
            threads.ForEach(t => t.Start());
            Console.WriteLine("Started the search and waiting results");
            //blocking till all threads finish
            threads.ForEach(t => t.Join());
            Console.WriteLine("Finished waiting");
        }

        static void SearchValue<T>(T[] bigArray,T whatToSearch, int minIndex, int maxIndex)
        {
            Console.WriteLine($"Searching {whatToSearch}...");
            for (int i = minIndex; i < maxIndex; i++)
            {
                if (whatToSearch.Equals(bigArray[i]))
                {
                    Console.WriteLine($"Found {whatToSearch} at {((float)(i-minIndex)/(maxIndex-minIndex)):P} of my piece");
                    return;
                }
            }
            Console.WriteLine($"Could not find {whatToSearch}");
        }

        /// <summary>
        /// Splitting array into pieces, getting a start-end indexes of every piece
        /// </summary>
        static List<Tuple<T, int, int>> GenerateRangeForValues<T>(T[] charArrayWithValues, int maxOffset)
        {
            var lowestOffset = maxOffset / charArrayWithValues.Length;
            return charArrayWithValues.Select((t, i) => new Tuple<T, int, int>(t, lowestOffset * i, lowestOffset * (i + 1))).ToList();
        }

    }
}