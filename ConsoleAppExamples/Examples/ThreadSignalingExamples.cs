﻿using System;
using System.Threading;

namespace ConsoleAppExamples.Examples
{
    public static class ThreadSignalingExamples
    {
        /// <summary>
        /// Simple signaling usage with AutoResetEvent
        /// </summary>
        public static void Example1()
        {
            var signal = new AutoResetEvent(false);
            string str = "";
            bool isThreadInTheBuidling = true;

            new Thread(() =>
            {
                Console.WriteLine("A Thread walks into the building...");
                while (!str.StartsWith("hello", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Thread: Hello, I am Thread!");
                    signal.WaitOne();
                }

                isThreadInTheBuidling = false;
                Console.WriteLine("Goodbye! Thread walks out...");
            }).Start();

            while (true)
            {
                str = Console.ReadLine();
                if (!isThreadInTheBuidling) break;
                signal.Set();
            }
            signal.Dispose();

        }

        /// <summary>
        /// CountdownEvent usage
        /// </summary>
        public static void Example2()
        {
            var cde = new CountdownEvent(4000);

            void func(object id)
            {
                var i = 0;
                while (true)
                {
                    Console.Write(id);
                    try
                    {
                        cde.Signal();
                    }
                    catch (InvalidOperationException)
                    {
                        break;
                    }
                    i++;

                    //uncomment this to increase granurality
                    //Thread.Sleep(0);
                }
                Console.Write($"\nThread({id}) signaled {i} times");
            }

            foreach (var sc in ".,;*-+:=^")
            {
                new Thread(func).Start(sc);
            }

            Console.Write("Start");
            cde.Wait();
            Console.Write("Finished");
        }
    }
}