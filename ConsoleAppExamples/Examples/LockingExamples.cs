﻿using System;
using System.Threading;

namespace ConsoleAppExamples.Examples
{
    public static class LockingExamples
    {
        /// <summary>
        /// Deadlock
        /// </summary>
        public static void Example1()
        {
            var o1 = new object();
            var o2 = new object();
            const string ThreadName1 = "Tim";
            const string ThreadName2 = "Allan";

            new Thread(() =>
            {
                Console.WriteLine(ThreadName2 + " started");
                lock (o1)
                {
                    Console.WriteLine(ThreadName2 + " passed lock o1");
                    Thread.Sleep(1000);
                    Console.WriteLine(ThreadName2 + " is going to enter lock o2");
                    lock (o2)
                    {
                        Console.WriteLine(ThreadName2 + " passed lock o2");
                    }
                }
                Console.WriteLine(ThreadName2 + " finished");
            }).Start();

            Console.WriteLine(ThreadName1 + " started");
            lock (o2)
            {
                Console.WriteLine(ThreadName1 + " passed lock o2");
                Thread.Sleep(1000);
                Console.WriteLine(ThreadName1 + " is going to enter lock o1");
                lock (o1)
                {
                    Console.WriteLine(ThreadName1 + " passed lock o1");
                }
            }
            Console.WriteLine(ThreadName1 + " finished");
        }
    }
}