﻿using System;
using System.Threading;

namespace ConsoleAppExamples
{
    public class LetterPrinter
    {
        public char Letter;

        public LetterPrinter(char letter)
        {
            Letter = letter;
        }

        public void Print()
        {
            for (int i = 0; i < 500; i++)
            {
                Console.Write(Letter);
            }
        }
    }
}
