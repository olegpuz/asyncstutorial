﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace AsyncStandupExamples.Example1
{
    /// <summary>
    /// Interaction logic for Example1.xaml
    /// </summary>
    public partial class Example1 : Window
    {
        #region Animation Settings

        private const int AnimationDelayMs = 40;
        private const int AnimationAngleIncrement = 5;
        private const int ColorAnimationRate = 10;
        
        #endregion

        #region State fields

        private readonly Random _randomizer = new Random();
        private int _currentColorAnimationIncrement = 0;


        #endregion
        

        public Example1()
        {
            InitializeComponent();

            Demo_4();
        }
        
        #region Demos

        /// <summary>
        /// Synchronized calls
        /// </summary>
        private void Demo_1()
        {
            StartButton.Click += (sender, args) =>
            {
                for (int i = 0; i < 100; i++)
                {
                    DoAnimation();
                    Thread.Sleep(AnimationDelayMs);
                }
            };
        }

        /// <summary>
        /// Another thread
        /// </summary>
        private void Demo_2()
        {
            StartButton.Click += (sender, args) =>
            {
                var thread = new Thread(() =>
                {
                    for (int i = 0; i < 100; i++)
                    {
                        DoAnimation();
                        Thread.Sleep(AnimationDelayMs);
                    }
                });

                thread.Start();
            };
        }
        
        /// <summary>
        /// Multi-threaded with UI thread
        /// </summary>
        private void Demo_3()
        {
            Thread thread = null;

            StartButton.Click += (sender, args) =>
            {
                thread = new Thread(() =>
                {
                    for (int i = 0; i < 100; i++)
                    {
                        Dispatcher.Invoke(DoAnimation);
                        Thread.Sleep(AnimationDelayMs);
                    }
                });
                thread.Start();
            };

            StopButton.Click += (sender, args) =>
            {
                if (thread == null) return;
                thread.Abort();
                thread = null;
            };
        }

        /// <summary>
        /// Async
        /// </summary>
        private void Demo_4()
        {
            bool isStopped;
            
            StartButton.Click += async (sender, args) =>
            {
                isStopped = false;
                for (int i = 0; i < 100; i++)
                {
                    if (isStopped) return;
                    DoAnimation();
                    await Task.Delay(AnimationDelayMs);
                }
            };

            StopButton.Click += (sender, args) => isStopped = true;
        }

        /// <summary>
        /// Task-based
        /// </summary>
        private void Demo_5()
        {
            Task mainTask;

            StartButton.Click += (sender, args) => 
            { 
                mainTask = Task.Run(() =>
                {
                    for (int i = 0; i < 100; i++)
                    {
                        DoAnimation();
                        Task.Delay(AnimationDelayMs).Wait();
                    }
                });
            };


        }

        #endregion

        #region Common logic

        private void DoAnimation()
        {
            var rotateElem = AnimationBox.RenderTransform as RotateTransform;
            rotateElem.Angle += AnimationAngleIncrement;

            _currentColorAnimationIncrement++;
            if (_currentColorAnimationIncrement % ColorAnimationRate == 0)
            {
                AnimationBox.Background = new SolidColorBrush(Color.FromRgb((byte) _randomizer.Next(255), (byte) _randomizer.Next(255),
                    (byte) _randomizer.Next(255)));
            }
        }

        #endregion
    }
}
